# NF/UserOnboarding: Custom tasks
This is an unaffiliated repo which contains some additional tasks for the User Onboarding addon.

If you're an addon developer and you'd like to create criteria tasks for the User Onboarding addon, feel free to open a pull request!

# Current tasks
* User has written an "About you"
* User has specified a location
* User has followed another user
* User has reacted to a post
* User has reacted to a specific post
* User is a member of the following primary usergroup
* User is a member of the following secondary usergroup
* User is using the following theme
* [tl] Social Groups: User has joined the following group
* User has made a post / message count is greater than 0
* User has verified their Xenforo license
* User has posted in a specific category
* User has posted at least X times