<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF\Entity\Post;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class PostedInCategory extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:PostedInCategory';
    }

    public function getOptionLabel()
    {
        return \XF::phrase('ap_ot_user_has_posted_in_category');
    }

    public function getOptions()
    {
        return [
            'node_id' => 'uint'
        ];
    }

    public function getConfigTemplateName()
    {
        return 'ap_ot_condition_config_posted_in_category';
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!\XF::visitor()->user_id
                    || \XF::visitor()->user_id != $entity->user_id)
                    {
                        return;
                    }

                    $this->matchConditions(['node_id' => $entity->Thread->Forum->Node->parent_node_id]);
                },
                'XF\Entity\Post'
            ]
        ];
    }

    public function getDisplayOrder()
    {
        return 1128;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn("
            SELECT DISTINCT p.user_id
            FROM xf_post AS p
                LEFT JOIN xf_thread AS t ON (t.thread_id = p.thread_id)
                LEFT JOIN xf_node AS n ON (n.node_id = t.node_id)
            WHERE p.user_id > ?
            AND n.parent_node_id = ?
            ORDER BY p.user_id
            LIMIT 500",
            [$position, $handlerData['options']['node_id']]);

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
