<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF\Entity\ReactionContent;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class ReactedToPost extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:ReactedToPost';
    }

    public function getOptionLabel()
    {
        return \XF::phrase('ap_ot_user_has_reacted_to_post');
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!\XF::visitor()->user_id
                    || \XF::visitor()->user_id != $entity->reaction_user_id)
                    {
                        return;
                    }

                    $this->matchConditions([]);
                },
                'XF\Entity\ReactionContent'
            ]
        ];
    }

    public function getDisplayOrder()
    {
        return 1119;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn("
            SELECT DISTINCT reaction_user_id
            FROM xf_reaction_content
            WHERE reaction_user_id > ?
            ORDER BY reaction_user_id
            LIMIT 500",
            [$position]);

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
