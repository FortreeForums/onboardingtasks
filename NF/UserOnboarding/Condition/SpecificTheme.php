<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF\Entity\User;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class SpecificTheme extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:SpecificTheme';
    }

    public function getOptionLabel()
    {
        return \XF::phrase('ap_ot_user_is_using_specific_theme');
    }

    public function getOptions()
    {
        return [
            'style_id' => 'uint'
        ];
    }

    public function getConfigTemplateName()
    {
        return 'ap_ot_condition_config_theme_specific';
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!\XF::visitor()->user_id
                    || \XF::visitor()->user_id != $entity->user_id)
                    {
                        return;
                    }

                    if($entity->style_id == 0)
                    {
                        $option = \XF::em()->find('XF:Option', 'defaultStyleId');

                        $styleId = $option->option_value;
                    }
                    else
                    {
                        $styleId = $entity->style_id;
                    }

                    $this->matchConditions(['style_id' => $styleId]);
                },
                'XF\Entity\User'
            ]
        ];
    }

    public function getDisplayOrder()
    {
        return 1124;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn("
            SELECT DISTINCT user_id
            FROM xf_user
            WHERE style_id = ?
            AND user_id > ?
            ORDER BY user_id
            LIMIT 500",
            [$handlerData['options']['style_id'], $position]);

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
