<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF\Entity\User;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class SecondaryGroup extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:SecondaryGroup';
    }

    public function getOptionLabel()
    {
        return \XF::phrase('ap_ot_user_is_member_of_secondary_group');
    }

    public function getOptions()
    {
        return [
            'secondary_group_id' => 'uint'
        ];
    }

    public function getConfigTemplateName()
    {
        return 'ap_ot_condition_config_secondary_group_specific';
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!\XF::visitor()->user_id
                    || \XF::visitor()->user_id != $entity->user_id)
                    {
                        return;
                    }

                    $this->matchConditions(['secondary_group_id' => $entity->secondary_group_ids]);
                },
                'XF\Entity\User'
            ]
        ];
    }

    public function getDisplayOrder()
    {
        return 1122;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn("
            SELECT DISTINCT user_id
            FROM xf_user
            WHERE secondary_group_ids LIKE CONCAT('%',?,'%')
            AND user_id > ?
            ORDER BY user_id
            LIMIT 500",
            [$handlerData['options']['secondary_group_id'], $position]);

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
