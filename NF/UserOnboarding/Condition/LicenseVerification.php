<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use LiamW\XenForoLicenseVerification\Entity\XenforoLicenseData;
use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class LicenseVerification extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:LicenseVerification';
    }

    public function getOptionLabel()
    {
        return \XF::phrase('ap_ot_user_has_verified_xenforo_license');
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!\XF::visitor()->user_id
                    || \XF::visitor()->user_id != $entity->user_id)
                    {
                        return;
                    }

                    if($entity->valid)
                    {
                        $this->matchConditions([]);
                    }
                },
                'LiamW\XenForoLicenseVerification\Entity\XenForoLicenseData'
            ]
        ];
    }

    public function isEnabled()
    {
        return isset(\XF::app()->container('addon.cache')['LiamW/XenForoLicenseVerification']);
    }

    public function getDisplayOrder()
    {
        return 1126;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn("
            SELECT DISTINCT user_id
            FROM xf_liamw_xenforo_license_data
            WHERE valid = 1
            AND user_id > ?
            ORDER BY user_id
            LIMIT 500", [$position]);

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
